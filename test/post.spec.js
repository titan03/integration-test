const request = require('supertest');
const app = require('../server');
const { Post } = require('../models');

describe('Posts API Collection', () => {
    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    describe('GET /api/v1/posts/', () => {
        test('Status code 200 should successfully get all data posts', (done) => {
            request(app)
                .get('/api/v1/posts/')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('posts')
                    done()
                }).catch(console.log);
        })

        test('Status code 404 should don`t get all data posts', (done) => {
            request(app)
                .get('/api/v1/posts/false')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(404)
                    done()
                }).catch(console.log);
        })
    })

    describe('POST /api/v1/posts/', () => {
        test('Status code 201 should succesfully create new data post', (done) => {
            request(app)
                .post('/api/v1/posts/')
                .set('Content-Type', 'application/json')
                .send({
                    title: 'Hello World',
                    body: 'Lorem Ipsum'
                })
                .then(res => {
                    expect(res.statusCode).toBe(201);
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('data');

                    expect(res.body.data.post)
                        .toEqual(
                            expect.objectContaining({
                                id: expect.any(Number),
                                title: expect.any(String),
                                body: expect.any(String),
                            })
                        );
                    done();
                }).catch(console.log)
        })

        test('Status code 422 should not create new data post', done => {
            request(app)
                .post('/api/v1/posts')
                .set('Content-Type', 'application/json')
                .send({ title: null, body: 'Lorem Ipsum' })
                .then(res => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('Fail');
                    done();
                }).catch(console.log);
        })
    })


    describe('PUT /api/v1/posts/:id', () => {
        test('Status code 202 should successfully updated data post', (done) => {
            request(app)
                .put('/api/v1/posts/1')
                .set('Content-Type', 'application/json')
                .send({
                    title: 'Hallo Human',
                    body: 'Lorem Ipsum'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data');
                    done()
                }).catch(console.log);
        })

        test('Status code 422 should not update data post', (done) => {
            request(app)
                .put('/api/v1/posts/1')
                .set('Content-Type', 'application/json')
                .send({
                    title: '',
                    body: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('Fail')
                    expect(res.body).toHaveProperty('error')
                    done()
                }).catch(console.log);
        })
    })


    describe('DELETE /api/v1/posts/:id', () => {
        test('Status code 200 should successfully deleted data post', (done) => {
            request(app)
                .delete('/api/v1/posts/1')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        })

        test('Status code 422 should not delete data post', (done) => {
            request(app)
                .delete(`/api/v1/posts/'1'`)
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('Fail')
                    expect(res.body).toHaveProperty('error')
                    done()
                }).catch(console.log);
        })
    })
})
