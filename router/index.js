const router = require('express').Router();
const IndexController = require('../controller/indexController');
const Post = require('../controller/PostController');

router.get('/', IndexController.home)

router.get('/api/v1/posts', Post.findAll)
router.post('/api/v1/posts', Post.create)
router.delete('/api/v1/posts/:id', Post.delete)
router.put('/api/v1/posts/:id', Post.update)

module.exports = router
